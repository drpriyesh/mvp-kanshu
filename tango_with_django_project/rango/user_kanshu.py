import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
# import django
# django.setup()

import glob
from django.contrib.auth.models import User
from rango.models import UserCategory, UserVocab, UserHSK, UserProfile, UserSource, UserSentence, UserPrevious
from rango.wechat_parser import wechat_parser
from requests.exceptions import ConnectionError
from collections import Counter
# from jieba import cut as j
# from jieba import posseg as pseg
# from googletrans import Translator
# from pinyin_jyutping_sentence import pinyin as pinyin
import re
import time
from populate.CEdict_parser import main
from datetime import datetime, timezone
import collections
from populate.pickle_dat import pickling, unpickling
import re
from empty_folder import clear

alphabets= "([A-Za-z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov)"

def split_into_sentences(text):

    text = " " + text + "  "
    text = text.replace("\n"," ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = re.sub(websites,"<prd>\\1",text)
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + alphabets + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + alphabets + "[.]"," \\1<prd>",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("<prd>",".")
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]
    return sentences


flag_ext = {'j':'Abbreviations',
                'g':'Academic vocabulary',
                'a':'Adjective',
                'al':'Adjective idiom',
                # 'ag':'Adjective morpheme',
                'ad':'Adverb',
                'd':'Adverb',
                'vd':'Adverb',
                'ag':'Adverbial',
                'dg':'Adverbial',
                # 'dg':'Adverbs',
                'nic':'Affiliates',
                'nis':'Agency suffix',
                'Rg':'Ancient Chinese pronoun morpheme',
                'nba':'Animal name',
                'uyy':'As common as',
                'ntcb':'Bank',
                'nb':'Biological name',
                'gb':'Biology related words',
                'gbc':'Biotype',
                'nbp':'Botanical name',
                'ude1':'Bottom of',
                'nmc':'Chemical name',
                'gc':'Chemistry related vocabulary',
                'wm':'Colon, full-width :: half-width::',
                'wd':'Comma, full-width :, half-width :,',
                'ntc':'Company Name',
                'nr1':'Compound surname',
                'gi':'Computer related vocabulary',
                'c':'Conjunction',
                'ul':'Conjunction',
                'uv':'Conjunction',
                'cc':'Coordinate conjunction',
                'wp':'Dash, full angle: —— —— —— － half angle: —— —',
                'rz':'Demonstrative',
                'nhd':'Disease',
                'bg':'Distinctive morpheme',
                'bl':'Distinguish part of speech idioms',
                'b':'Distinguishing words',
                'nhm':'Drug',
                'nit':'Education related institutions',
                'nts':'Elementary and secondary schools',
                'ws':'Ellipses, full-width: ...',
                'wt':'Exclamation mark, full-width :!',
                'ntcf':'Factory',
                'nf':'Food, such as "potato chips"',
                'vx':'Formal verb',
                'wj':'Full stop, full angle :.',
                'gg':'Geology and Geology',
                'ude3':'Get',
                'nto':'Government agency',
                'ude2':'Ground',
                'nh':'Health related terms such as medical diseases',
                'nth':'Hospital',
                'ntch':'Hotel Guest House',
                'i':'Idiom',
                'l':'Idiom',
                'udh':'if',
                'uls':'In terms of speaking',
                # 'nt':'Institution name',
                'nt':'Institutional groups',
                'ni':'Institution-related (not the name of an independent institution)',
                'e':'Interjection',
                'rys':'Interrogative pronoun',
                'ry':'Interrogative pronouns',
                'vi':'Intransitive verb (internal verb)',
                'nm':'Item name',
                'nrj':'Japanese name',
                'nn':'Job-related nouns',
                'wkz':'Left bracket, full angle: (〔[{{"" [〖<half angle: ([{<',
                # 'wyz':'Left quote, full angle: "'『',
                'nx':'Letter proper name',
                'ulian':'Lian ("Even Primary School Students")',
                'dl':'Link',
                's':'Location word',
                'gm':'Math related vocabulary',
                # 'y':'Modal',
                'y':'Modal particle (delete yg)',
                'nr2':'Mongolian name',
                # 'g':'Morpheme',
                # 'vg':'Morpheme',
                'nr':'Name',
                'an':'Nominal form',
                'nl':'Nominal idiom',
                'ng':'Nominal morpheme',
                'vn':'Nominal verb',
                # 'x':'Non-morpheme',
                'xx':'Non-morpheme',
                'n':'Noun',
                'mg':'Number morpheme',
                'm':'Numeral',
                'Mg':'Numerals like A, B, C',
                'nnd':'Occupation',
                'uzhi':'Of',
                'ule':'Okay',
                'end':'Only for final ## Final',
                'o':'Onomatopoeia',
                'nz':'Other proper names',
                'u':'Particle',
                'ud':'Particle',
                'uj':'Particle',
                'ug':'Past',
                'uguo':'Past',
                'wb':'Percent sign, thousand sign, full angle:% ‰ half angle:%',
                'rr':'Personal Pronouns',
                'gp':'Physics related vocabulary',
                'usuo':'Place',
                'ns':'Place name',
                'nnt':'Position / Title',
                'f':'Position of the word',
                # 'h':'Predecessor',
                'rzv':'Predicate demonstrative pronoun',
                'ryv':'Predicate interrogative pronoun',
                'h':'Prefix',
                'rzs':'Premise pronoun',
                'p':'Preposition',
                'r':'Pronoun',
                'rg':'Pronoun morpheme',
                'w':'Punctuation',
                'q':'quantifier',
                'mq':'Quantifier1',
                'qg':'Quantifier2',
                'qt':'Quantifier3',
                'qv':'Quantifier4',
                'ww':'Question mark, full-width :?',
                'wky':'Right parenthesis, full-width :)〕]｝》】〉 Half-width:)] {>',
                # 'wyy':'Right quote, full-width: "'』',
                'wf':'Semicolon, full-width :; half-width :;',
                'z':'Status word',
                'zg':'Status word',
                'x':'String',
                # 'k':'Subsequent ingredients',
                'k':'Suffix',
                'tg':'Temporal part-of-speech',
                # 'tg':'Tenor',
                'pba':'The preposition "Ba"',
                'pbei':'The preposition "be"',
                'ntu':'the University',
                'vyou':'The verb "有"',
                'ryt':'Time interrogative pronoun',
                'rzt':'Time pronoun',
                't':'Time word',
                'wn':'Ton, full-width :,',
                'yg':'Tone morpheme',
                'nsf':'Transliterated place names',
                'nrf':'Transliteration',
                'vf':'Trending verb',
                'wh':'Unit symbol, full-width: ¥ ＄ ￡ ° ℃ half-width: $',
                'un':'Unknown word',
                'xu':'URL URL',
                'v':'Verb',
                'vshi':'Verb "Yes"',
                'vl':'Verb idiom',
                'vg':'Verb morpheme',
                'udeng':'Wait wait wait',
                'uz':'Write',
                'uzhe':'Write',
                'nbc':'Zoology'
                }
CEdict = main()

def doc_process(user):

    from jieba_fast import cut as j
    from jieba_fast import posseg as pseg
    from googletrans import Translator
    from pinyin_jyutping_sentence import pinyin as pinyin
    create_list=[]
    doc = UserProfile.objects.get(user=user)
    doc_path = doc.document.path

    base = os.path.basename(doc_path)
    source_name = os.path.splitext(base)[0]

    # this is the location of the users pickle dumped data
    pickle_path = os.path.dirname(os.path.abspath(doc_path))
    pickle_path = os.path.join(pickle_path, 'pickle_files/' + source_name + '/')

    if not os.path.exists(pickle_path):
        os.makedirs(pickle_path)

    print("Opening input file...")
    with open(doc_path, encoding="utf8", errors='ignore') as f:
        inputSample = f.read()


    inputSample_list = j(inputSample, cut_all=False)
    inputSampleCnt = dict(Counter(inputSample_list))

    print('Identifying categories...')
    posDict = unpickling(pickle_path + source_name + 'flags')

    if not posDict['data']:
        words = pseg.cut(inputSample)
        for w in words:
            posDict[w.word] = w.flag

        pickling(posDict, pickle_path + source_name + 'flags')

    try:
        picklecount = unpickling(pickle_path + source_name + 'count')['data'] #.pop()
    except TypeError:
        picklecount = 0

    if not picklecount:
        picklecount = 0

    vocab = unpickling(pickle_path + source_name + 'vocab')['data']
    if not vocab:
        vocab = []

    sorted_inputSampleCnt = sorted(inputSampleCnt.items(), key=lambda kv: kv[1], reverse=True)

    # ===================================================================
    # ===================================================================
    # ===================================================================
    print("Creating vocabulary dictionary...")

    count = 0

    for x, freq in sorted_inputSampleCnt:

        if re.search("[\u4e00-\u9FFF]", x):

            count += 1

            if int(picklecount) < count:

                try:

                    CEdict_translated = next(item for item in CEdict if item["simplified"] == x)
                    k = CEdict_translated['english']
                except StopIteration:
                    k = x
                    pass

                pY = pinyin(x, spaces=False)

                try:
                    curr_cat = posDict[x]
                except KeyError:
                    curr_cat = 'other'

                att = {'category': curr_cat, 'chinese': x, 'score': 0, 'frequency': int(freq), 'english': k, 'previous': 0, 'pinyin': pY, 'learnt': 0}
                vocab.append(att)

                pickling(vocab, pickle_path + source_name + 'vocab')
                pickling(count, pickle_path + source_name + 'count')


    print('Source name: ' + source_name[0:6])
    s = add_usersource(user, source_name)
    print("Finished creating vocab dict (pickling)...")

    # ===================================================================
    # ===================================================================
    # ===================================================================
    print("Creating sentences...")
    if source_name[0:6] == 'wechat':
        print("Wechat detected...")
        w = wechat_parser(user, s, doc_path)

        return source_name
    else:
        sentenceDict = unpickling(pickle_path + source_name + 'sentences')

        if not sentenceDict['data']:
            sentenceDict = []

        iTok = split_into_sentences(inputSample)
        count = 0
        sentence_count = 1
        for sentence in iTok:

            # categorize
            posDict = {}
            words = pseg.cut(sentence)
            for w in words:
                posDict[w.word] = w.flag

            # pinyin
            pYa = pinyin(sentence, spaces=False)
            pYa = pYa.replace(',', ' ')
            pYa = pYa.replace('  ', '')
            pYa = pYa.strip(' .')
            pYa = pYa.replace(' ', ',')
            pY = pYa.replace(',', '\t')  # this is pinyin sentence for display

            # # google trans
            # gTranslated = g(sentence).text # this is google translated sentence for display
            gTranslated = ''

            # jieba
            seg_list = j(sentence, cut_all=False)
            segStr = (",".join(seg_list))
            segStr = segStr.replace(',', ' ')
            segStr = segStr.replace('  ', '')
            segStr = segStr.strip(' .')
            segStr = segStr.replace(' ', ',')
            segSent = (segStr.replace(',', '\t'))  # this is chinese sentence for display

            # print(bcolors.HEADER + segSent + bcolors.ENDC)

            # making list of words
            # pinyin list
            pyList = pYa.split(",")
            # chinese list
            segStrList = segStr.split(",")


            pickling(sentence, pickle_path + source_name + 'sentences')

            # us = add_usersentence(user, s, segSent, pY, gTranslated, sentence_count)
            ## us.source.add(source_name)
            sdict = {'user':user, 'source':s, 'chinese':segSent, 'pinyin':pY, 'english':gTranslated, 'number':sentence_count}
            create_list.append(sdict)

            sentence_count = sentence_count + 1

    if create_list:
        batch = 999
        q = len(create_list) // batch
        r = len(create_list) % batch
        ss = len(create_list) - r
        if len(create_list) > batch:


            for i in range(0, q):
                start = batch * i
                end = (batch * i) + batch
                print('q:' + str(q) + ' i:' + str(i) + ' start:' + str(start) + ' -> end:' + str(end))
                UserSentence.objects.bulk_create([UserSentence(**kv) for kv in create_list[start:end]])

        print('last start:' + str(ss) + '-> end:' + str(len(create_list)))

        UserSentence.objects.bulk_create([UserSentence(**kv) for kv in create_list[ss:len(create_list)]])

        print('Finished pe-processing.')

        return source_name




def binfile_to_db(user):
    create_list=[]
    # this is the base directory of the project
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # this is where the HSK and CEdict files exist
    POP_DIR = os.path.join(BASE_DIR, 'populate/')

    doc = UserProfile.objects.get(user=user)
    doc_path = doc.document.path
    base = os.path.basename(doc_path)
    source_name = os.path.splitext(base)[0]
    s = add_usersource(user, source_name)
    print('source_name: ' + source_name)

    # this is the location of the users pickle dumped data
    pickle_path = os.path.dirname(os.path.abspath(doc_path))
    pickle_path = os.path.join(pickle_path, 'pickle_files/' + source_name + '/')

    vocab = unpickling(pickle_path + source_name + 'vocab')['data']
    if not vocab:
        vocab = []


    hsk = {}
    with open(POP_DIR + 'HSK_Official_2012_L1.txt', encoding="utf8", errors='ignore') as f1:
        text = f1.read()
        lines = text.split('\n')
        dict_lines = list(lines)
        hsk['1'] = dict_lines
    with open(POP_DIR + 'HSK_Official_2012_L2.txt', encoding="utf8", errors='ignore') as f2:
        text = f2.read()
        lines = text.split('\n')
        dict_lines = list(lines)
        hsk['2'] = dict_lines
    with open(POP_DIR + 'HSK_Official_2012_L3.txt', encoding="utf8", errors='ignore') as f3:
        text = f3.read()
        lines = text.split('\n')
        dict_lines = list(lines)
        hsk['3'] = dict_lines
    with open(POP_DIR + 'HSK_Official_2012_L4.txt', encoding="utf8", errors='ignore') as f4:
        text = f4.read()
        lines = text.split('\n')
        dict_lines = list(lines)
        hsk['4'] = dict_lines
    with open(POP_DIR + 'HSK_Official_2012_L5.txt', encoding="utf8", errors='ignore') as f5:
        text = f5.read()
        lines = text.split('\n')
        dict_lines = list(lines)
        hsk['5'] = dict_lines
    with open(POP_DIR + 'HSK_Official_2012_L6.txt', encoding="utf8", errors='ignore') as f6:
        text = f6.read()
        lines = text.split('\n')
        dict_lines = list(lines)
        hsk['6'] = dict_lines

    vocab_hsk = []

    print("Adding hsk level to vocab dict...")
    for i in range(1, 7):
        for word in hsk[str(i)]:
            for item in vocab:

                if item['chinese'] == word:
                    item['hsk'] = i

                    vocab_hsk.append(item)

    print("Grouping vocabulary to categories...")
    grouped = collections.defaultdict(list)
    for item in vocab:
        grouped[item['category']].append(item)

    cats = {}
    for category, group in grouped.items():
        cats[category] = {'vocabulary': group}

    for cat, cat_data in cats.items():
        try:
            cat = flag_ext[cat]
        except KeyError:
            cat = 'other'
        c = add_usercat(user, cat)

        for p in cat_data['vocabulary']:
            if "hsk" not in p:
                p['hsk'] = '0'
            h = add_userhsk(user, p['hsk'])
            vdict = {'source': s, 'user':user, 'category': c, 'hsk': h, 'chinese': p['chinese'], 'english': p['english'],
                     'pinyin': p['pinyin'], 'learnt': p['learnt'], 'score': p['score'], 'previous': p['previous'],
                     'frequency': p['frequency'], 'hsk_lvl': p['hsk']}
            create_list.append(vdict)
            # v = add_uservocab(s, user, c, h, p['chinese'], p['english'], p['pinyin'], p['learnt'], p['score'], p['previous'],
            #           p['frequency'], p['hsk'])
    if create_list:
        batch = 999
        q = len(create_list) // batch
        r = len(create_list) % batch
        ss = len(create_list) - r
        if len(create_list) > batch:


            for i in range(0, q):
                start = batch * i
                end = (batch * i) + batch
                print('q:' + str(q) + ' i:' + str(i) + ' start:' + str(start) + ' -> end:' + str(end))
                UserVocab.objects.bulk_create([UserVocab(**kv) for kv in create_list[start:end]])

        print('last start:' + str(ss) + '-> end:' + str(len(create_list)))

        UserVocab.objects.bulk_create([UserVocab(**kv) for kv in create_list[ss:len(create_list)]])

    # Print out the categories we have added.
    for c in UserCategory.objects.all():
        for p in UserVocab.objects.filter(category=c):
            print(f'- {c}: {p}')


    try:
        print('Emptying pickle folder...')
        clear(pickle_path)
    except:
        print('Failed to empty pickle folder...')
        pass
    # files = glob.glob(pickle_path)
    # for f in files:
    #     os.remove(f)

    print('Finished adding data to user database.')

def run_user_kanshu(sentence_number, user, source):
    # from googletrans import Translator
    # translator = Translator()
    # google_translate = translator.translate

    source_id = UserSource.objects.get(user=user, slug=source)
    # print('source_id: ' + str(source_id))
    # print('source_id.id: ' + str(source_id.id))
    # print('source_id.title: ' + source_id.title)
    # print('user.id: ' + str(user.id))

    # changed
    original_sentence_number = sentence_number
    sentence_number = int(sentence_number)
    result = list(UserSentence.objects.filter(user=user.id, source=source_id.id).values('user', 'source', 'number', 'chinese', 'pinyin', 'english')[sentence_number-1:sentence_number+4])
    result_a = [results for results in result if results['user'] == user.id]
    results = [results for results in result_a if results['source'] == source_id.id]
    sorted(results, key=lambda i: i['number'])

    side_word_list = []
    vlist = list(UserVocab.objects.filter(source=source_id, user=user).values('user', 'chinese', 'pinyin', 'english', 'frequency'))
    plist = list(UserPrevious.objects.filter(user=user).values('id', 'user', 'chinese', 'previous', 'updated', 'learnt', 'slug'))
    update_list = []
    create_list = []
    for result in results:

        t_original = result['chinese']
        sentence_vocab = t_original.split("\t")

        for word in sentence_vocab:

            try:
                now = datetime.now(timezone.utc)
                # new
                vocab_index = next(item for item in vlist if item['chinese'] == word)
                english = vocab_index['english']
                chinese = vocab_index['chinese']
                pinyin = vocab_index['pinyin']
                frequency = vocab_index['frequency']
                try:
                    pdict = next(item for item in plist if item['chinese'] == word)

                    updated_time = pdict['updated']
                    if (now - updated_time).seconds > 20 and (now - updated_time).days <= 5:
                        previous = pdict['previous'] + 1
                    elif (now - updated_time).days > 5:
                        previous = 0
                    else:
                        previous = pdict['previous']
                    learnt = pdict['learnt']
                    id = pdict['id']
                    pchinese = pdict['chinese']
                    pid = pdict['user']
                    update_dict = {'id': pid, 'chinese': pchinese, 'previous': previous, 'updated': now, 'learnt': learnt}
                    update_list.append(update_dict)
                except StopIteration:
                    previous = 0
                    learnt = 1
                    id = ''
                    create_dict = {'chinese': chinese, 'user': user , 'learnt': learnt}
                    create_list.append(create_dict)

                side_word_dict = {'pinyin': pinyin, 'english': english, 'chinese': chinese,
                                  'sentence_number': original_sentence_number, 'frequency': frequency,
                                  'previous': previous, 'learnt':learnt, 'id':  id}

            except StopIteration:

                side_word_dict = {'pinyin': word, 'english': word, 'chinese': word,
                                  'sentence_number': original_sentence_number, 'frequency': 0, 'learnt': 1, 'id': ''}

            side_word_list.append(side_word_dict)
        side_word_dict = {'pinyin': '。', 'english': '。', 'chinese': '。',
                          'sentence_number': '。', 'frequency': 0, 'learnt': 1, 'id': ''}

        side_word_list.append(side_word_dict)

    if update_list:
        UserPrevious.objects.bulk_update([UserPrevious(**kv) for kv in update_list], ['previous'])

    if create_list:
        UserPrevious.objects.bulk_create([UserPrevious(**q) for q in create_list])

    results = {'results': results, 'sides': side_word_list}

    return results




def add_uservocab(source, user, category, hsk, chinese, english, pinyin, learnt=0, score=0, previous=0, frequency=0, hsk_lvl=0):
    p = UserVocab.objects.get_or_create(source=source, user=user, category=category, hsk=hsk, chinese=chinese)[0]
    p.english=english
    p.pinyin=pinyin
    p.score=score
    p.frequency=frequency
    p.previous=previous
    p.learnt=learnt
    p.hsk_lvl=hsk_lvl
    p.save()
    return p


def add_usercat(user, name):
    c = UserCategory.objects.get_or_create(user=user, name=name)[0]
    c.save()
    return c

def add_userhsk(user, name):
    h = UserHSK.objects.get_or_create(user=user, hsk_level=name)[0]
    h.save()
    return h

# add user source details to user vocabulary
def add_usersource(user, name):
    s = UserSource.objects.get_or_create(user=user, title=name)[0]
    s.save()
    return s

def add_usersentence(user, source, chinese, pinyin, english, number):
    l = UserSentence.objects.get_or_create(user=user, source=source, chinese=chinese, pinyin=pinyin, english=english)[0]
    l.number = number
    l.save()
    return l

def add_userprevious(user, chinese):
    p = UserPrevious.objects.get_or_create(user=user, chinese=chinese)[0]
    p.save()
    return p

