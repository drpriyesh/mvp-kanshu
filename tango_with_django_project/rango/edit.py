# " jieba.load_userdict(file_name) # file_name is the path dictionary format of a file-type object or a custom dictionary.
# It is the same as dict.txt. One word Occupy one line; each line is divided into three parts: words, word frequency (can be omitted),
# part of speech (can be omitted), separated by spaces, and the order cannot be reversed.
# If file_name is a file opened in path or binary mode, the file must be UTF-8 encoded.
# When the word frequency is omitted, the automatically calculated word frequency that can guarantee the separation of the word is used."

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')

from rango.models import Sentence, Source, HSK, Category, Vocab, UserSource, UserSentence, UserSource, UserHSK, UserCategory, UserVocab
from rango.wechat_parser import wechat_parser_edit
import jieba
from jieba import cut as j
import jieba.posseg as pseg
from pinyin_jyutping_sentence import pinyin as pinyin


def editword(source, words):
    update_list = []
    source_title = source['title']
    source_id = source['source_id']
    # jieba.enable_paddle()

    # dict_path = "C:/Users/bzgl6w/PycharmProjects/rango/tango_with_django_project/rango/customdict.txt"
    # jieba.load_userdict(dict_path)


    word = words['word']
    pin = pinyin(word, spaces=False)

    # freq = int(word['freq'])
    tag = words['tag']
    english = tag +': ' +words['english']
    h = HSK.objects.get_or_create(hsk_level=0)[0]
    h.save()
    c = Category.objects.get_or_create(name=tag)[0]
    c.save()
    v = Vocab.objects.get_or_create(source=source_id, category=c, hsk=h, chinese=word, pinyin=pin, english=english)[0]
    v.save()
    jieba.add_word(word, freq=None, tag=tag)

    olist = list(Sentence.objects.filter(source=source_id).values('id', 'source', 'chinese', 'pinyin'))


    plist = list(Sentence.objects.filter(source=source_id).values('id', 'source', 'chinese', 'pinyin'))
    # print(plist[:10])
    count=0
    for line in plist:

        oline = olist[count]
        # print(line['chinese'])
        l = line['chinese']
        l = l.replace('\t', '')
        # print(l)
        l = l.replace('\t', '')

        # print(l)
        seg_list = jieba.cut(l, cut_all=False)
        segStr = (",".join(seg_list))
        segStr = segStr.replace(',', ' ')
        segStr = segStr.replace('  ', '')
        segStr = segStr.strip(' .')
        segStr = segStr.replace(' ', ',')
        segSent = (segStr.replace(',', '\t'))  # this is chinese sentence for display
        line['chinese']=segSent
        # print(line['chinese'])
        # print(oline['chinese'])
        pYa = pinyin(l, spaces=False)
        pYa = pYa.replace(',', ' ')
        pYa = pYa.replace('  ', '')
        pYa = pYa.strip(' .')
        pYa = pYa.replace(' ', ',')
        pY = pYa.replace(',', '\t')
        line['pinyin'] = pY


        if segSent != oline['chinese']:

            # print(oline['chinese'])
            # print(line['chinese'])
            update_dict = {'id': oline['id'], 'chinese': line['chinese'], 'pinyin': line['pinyin']}
            update_list.append(update_dict)
        count+=1


    if update_list:
        batch = 100
        q = len(update_list) // batch
        r = len(update_list) % batch
        s = len(update_list) - r
        if len(update_list) > batch:


            for i in range(0,q):
                start = batch*i
                end = (batch*i)+batch
                print('q:'+str(q) +' i:'+str(i) +' start:'+ str(start) + ' -> end:'+ str(end))
                Sentence.objects.bulk_update([Sentence(**kv) for kv in update_list[start:end]], ['chinese', 'pinyin'])

        print('last start:' + str(s) + '-> end:' + str(len(update_list)))

        Sentence.objects.bulk_update([Sentence(**kv) for kv in update_list[s:len(update_list)]], ['chinese', 'pinyin'])

    print('done')

# if __name__=='__main__':
#
#     print('Starting Kanshu sentence edit script...')
#     words = []
#     words.append({'word':'达德里', 'freq':'10000000000', 'tag':'Name', 'english':'Dudley'})
#     words.append({'word':'哈利波特', 'freq':'1000000000000', 'tag':'Name', 'english':'Harry Potter'})
#     words.append({'word':'哈格力', 'freq':'1000000000000', 'tag':'Name', 'english':'Hagrid'})
#     print(words)
#
#     edit("Harry_Potter_Mandarin", words)
def usereditword(user, source, word, category):
    update_list = []

    pin = pinyin(word, spaces=False)

    jieba.add_word(word, freq=None, tag=category)

    olist = list(UserSentence.objects.filter(user_id=user.id, source_id=source.id).values('id', 'source', 'chinese', 'pinyin'))
    plist = list(UserSentence.objects.filter(user_id=user.id, source_id=source.id).values('id', 'source', 'chinese', 'pinyin'))
    # print(olist[:10])

    count=0
    for line in plist:

        oline = olist[count]
        # print(line['chinese'])
        l = line['chinese']
        l = l.replace('\t', '')
        # print(l)
        l = l.replace('\t', '')
        # print('sentence:' +l)
        if not source.title[0:6] == 'wechat':
            print('not wechat...')
            seg_list = jieba.cut(l, cut_all=False)
            segStr = (",".join(seg_list))
            segStr = segStr.replace(',', ' ')
            segStr = segStr.replace('  ', '')
            segStr = segStr.strip(' .')
            segStr = segStr.replace(' ', ',')
            segSent = (segStr.replace(',', '\t'))  # this is chinese sentence for display
            line['chinese']=segSent
            # print(line['chinese'])
            # print(oline['chinese'])
            pYa = pinyin(l, spaces=False)
            pYa = pYa.replace(',', ' ')
            pYa = pYa.replace('  ', '')
            pYa = pYa.strip(' .')
            pYa = pYa.replace(' ', ',')
            pY = pYa.replace(',', '\t')
            line['pinyin'] = pY

        else:

            print('Editing wechat source...')
            we_edit = wechat_parser_edit(l, word, category)
            # print(we_edit)
            segSent = we_edit['chsentence']
            pY = we_edit['pysentence']

        if segSent != oline['chinese']:

            print('original: '+oline['chinese'])
            print('segSent: '+segSent)
            update_dict = {'id': oline['id'], 'chinese': segSent, 'pinyin': pY}
            update_list.append(update_dict)

        count+=1


    if update_list:
        batch = 100
        q = len(update_list) // batch
        r = len(update_list) % batch
        s = len(update_list) - r
        if len(update_list) > batch:


            for i in range(0,q):
                start = batch*i
                end = (batch*i)+batch
                print('q:'+str(q) +' i:'+str(i) +' start:'+ str(start) + ' -> end:'+ str(end))
                UserSentence.objects.bulk_update([UserSentence(**kv) for kv in update_list[start:end]], ['chinese', 'pinyin'])

        print('last start:' + str(s) + '-> end:' + str(len(update_list)))

        UserSentence.objects.bulk_update([UserSentence(**kv) for kv in update_list[s:len(update_list)]], ['chinese', 'pinyin'])

    print('done')
    return pin