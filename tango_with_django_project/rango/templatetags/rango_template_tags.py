from django import template
from rango.models import Category, Vocab, Sentence


register=template.Library()

@register.inclusion_tag('rango/categories.html')
def get_category_list(current_category=None):
    return {'categories': Category.objects.order_by('-likes')[:10], 'current_category': current_category}


