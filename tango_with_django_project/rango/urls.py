from django.urls import path
from rango import views

app_name = 'rango'


urlpatterns = [
path('', views.cover, name='cover'),
path('bookshelf/', views.bookshelf, name='bookshelf'),
path('bookshelf_user/', views.bookshelf_user, name='bookshelf_user'),
path('index/<slug:source_title_slug>/', views.index, name='index'),
path('about/', views.about, name='about'),
path('contact_us/', views.contact_us, name='contact_us'),
path('category/<slug:category_name_slug>/', views.show_category, name='show_category'),
path('add_category/', views.add_category, name='add_category'),
path('category/<slug:category_name_slug>/add_vocabulary/', views.add_vocabulary, name='add_vocabulary'),
path('suggest/', views.CategorySuggestionView.as_view(), name='suggest'),
# path('register/', views.register, name='register'),
# path('login/', views.user_login, name='login'),
path('restricted/', views.restricted, name='restricted'),
path('sentence/', views.show_sentence, name='show_sentence'),
# path('kanshu/', views.kanshu, name='kanshu'),
path('like_category/', views.like_category, name='like_category'),
path('bookmark/', views.bookmark, name='bookmark'),
path('add_word2dict/', views.add_word2dict, name='add_word2dict'),
path('del_word2dict/', views.del_word2dict, name='del_word2dict'),
path('words_in_sent/', views.words_in_sent, name='words_in_sent'),
path('learnt/', views.learnt, name='learnt'),
path('edit/', views.edit, name='edit'),
path('useredit/', views.useredit, name='useredit'),
# path('next_kanshu/', views.next_kanshu, name='next_kanshu'),
path('kanshu_sentence/<slug:source_title_slug>/', views.kanshu_sentence, name='kanshu_sentence'),
path('kanshu_sentence2/<slug:source_title_slug>/', views.mother_text_helper_sentence, name='mother_text_helper_sentence'),
path('kanshu_carousel/<slug:source_title_slug>/', views.kanshu_carousel, name='kanshu_carousel'),
path('kanshu_carousel2/<slug:source_title_slug>/', views.mother_text_helper, name='mother_text_helper'),
# path('personal_kanshu/', views.personal_kanshu, name='personal_kanshu'),
path('personal_kanshu/<slug:source_title_slug>/', views.personal_kanshu_source, name='personal_kanshu_source'),

path('personal_kanshu_source_delete/<slug:source_title_slug>/', views.personal_kanshu_source_delete, name='personal_kanshu_source_delete'),
path('personal_kanshu_index_delete/', views.personal_kanshu_index_delete, name='personal_kanshu_index_delete'),

path('personal_kanshu_index/', views.personal_kanshu_index, name='personal_kanshu_index'),
path('kanshu_dashboard/', views.kanshu_dashboard, name='kanshu_dashboard'),
path('register_profile/', views.register_profile, name='register_profile'),
path('profile/<username>/', views.ProfileView.as_view(), name='profile'),
# path('logout/', views.user_logout, name='logout'),
]

