import datetime
from django.db import models
from django.utils import timezone
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.contrib import admin
import re


class Source(models.Model):
    class Meta:
        verbose_name_plural = 'Sources'

    TITLE_MAX_LENGTH = 511

    title = models.CharField(max_length=TITLE_MAX_LENGTH)

    def user_directory_path(instance, filename):
        # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
        return 'title_{0}/{1}'.format(instance.title, filename)

    language = models.CharField(max_length=TITLE_MAX_LENGTH, default='')
    author = models.CharField(max_length=TITLE_MAX_LENGTH)  # e.g. author or text
    type = models.CharField(max_length=TITLE_MAX_LENGTH)  # e.g. book, movie subs, emails, user
    picture = models.ImageField(upload_to=user_directory_path, blank=True)
    url = models.URLField()
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Source, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class HSK(models.Model):
    NAME_MAX_LENGTH = 4
    hsk_level = models.CharField(max_length=NAME_MAX_LENGTH, unique=True)

    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.hsk_level)
        super(HSK, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'HSK'

    def __str__(self):
        return self.hsk_level


class UserHSK(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    NAME_MAX_LENGTH = 4
    hsk_level = models.CharField(max_length=NAME_MAX_LENGTH, unique=False)
    slug = models.SlugField(unique=True)

    class Meta:
        verbose_name_plural = 'User HSK'

    # def save(self, *args, **kwargs):
    #
    #     self.slug = slugify(self.hsk_level)
    #     super(UserHSK, self).save(*args, **kwargs)

    def save(self, **kwargs):
        slug_str = "%s %s" % (self.user, self.hsk_level)
        unique_slugify(self, slug_str)
        super(UserHSK, self).save()

    def __str__(self):
        return self.hsk_level


class Category(models.Model):
    NAME_MAX_LENGTH = 128
    sources = models.ManyToManyField(Source)

    def get_sources(self):
        return ",".join([str(p) for p in self.sources.all()])

    name = models.CharField(max_length=NAME_MAX_LENGTH, unique=True)
    pub_date = models.DateTimeField('date published', auto_now=True)
    slug = models.SlugField()  # unique=True
    likes = models.IntegerField(default=0)
    hsk = models.ManyToManyField(HSK, related_name='word', through='Vocab')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

    def was_published_recently(self):
        return self.pub_date >= (timezone.now() - datetime.timedelta(days=1)).timestamp()
        # return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


class UserCategory(models.Model):
    sources = models.ManyToManyField(Source)

    def get_sources(self):
        return ",".join([str(p) for p in self.sources.all()])

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    NAME_MAX_LENGTH = 128
    name = models.CharField(max_length=NAME_MAX_LENGTH)
    pub_date = models.DateTimeField('date published', auto_now=True)
    slug = models.SlugField(unique=True)  # unique=True
    likes = models.IntegerField(default=0)
    hsk = models.ManyToManyField(UserHSK, related_name='word', through='UserVocab')

    # def save(self, *args, **kwargs):
    #
    #     self.slug = slugify(self.name)
    #     super(UserCategory, self).save(*args, **kwargs)

    def save(self, **kwargs):
        slug_str = "%s %s" % (self.user, self.name)
        unique_slugify(self, slug_str)
        super(UserCategory, self).save()

    class Meta:
        verbose_name_plural = 'User Categories'

    def __str__(self):
        return self.name

    def was_published_recently(self):
        return self.pub_date >= (timezone.now() - datetime.timedelta(days=1)).timestamp()
        # return self.pub_date >= timezone.now() - time.timedelta(days=1)


class UserPrevious(models.Model):
    class Meta:
        verbose_name_plural = 'User Previous'

    TITLE_MAX_LENGTH = 128
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    chinese = models.CharField(max_length=TITLE_MAX_LENGTH)
    previous = models.IntegerField(default=0)
    sentence_num = models.IntegerField(default=0)
    updated = models.DateTimeField('date updated', auto_now=True)
    learnt = models.IntegerField(default=0)
    slug = models.SlugField(unique=False)

    def save(self, **kwargs):
        slug_str = "%s %s" % (self.user, self.chinese)
        unique_slugify(self, slug_str)
        super(UserPrevious, self).save()

    def __str__(self):
        return self.chinese


class Word(models.Model):
    TITLE_MAX_LENGTH = 128
    traditional = models.CharField(max_length=TITLE_MAX_LENGTH)
    simplified = models.CharField(max_length=TITLE_MAX_LENGTH)
    english = models.CharField(max_length=TITLE_MAX_LENGTH)
    pinyin = models.CharField(max_length=TITLE_MAX_LENGTH)


class Vocab(models.Model):
    class Meta:
        verbose_name_plural = 'Vocabulary'

    TITLE_MAX_LENGTH = 128
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    hsk = models.ForeignKey(HSK, on_delete=models.CASCADE)  # , null=True, blank=True)
    chinese = models.CharField(max_length=TITLE_MAX_LENGTH)
    pinyin = models.CharField(max_length=TITLE_MAX_LENGTH)
    english = models.CharField(max_length=TITLE_MAX_LENGTH)
    frequency = models.IntegerField(default=0)
    previous = models.IntegerField(default=0)
    score = models.IntegerField(default=0)
    learnt = models.IntegerField(default=0)
    hsk_lvl = models.IntegerField(default=0)
    slug = models.SlugField()  # unique=True

    def save(self, *args, **kwargs):
        self.slug = slugify(self.pinyin)
        super(Vocab, self).save(*args, **kwargs)

    def __str__(self):
        return self.chinese


class UserBookmark(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    bookmark = models.IntegerField(default=1)
    adjust = models.IntegerField(default=0)

    def __str__(self):
        return self.bookmark


class UserDictionary(models.Model):

    TITLE_MAX_LENGTH = 255

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    source = models.CharField(max_length=TITLE_MAX_LENGTH)
    hsk = models.IntegerField(default=0) # , null=True, blank=True)
    chinese = models.CharField(max_length=TITLE_MAX_LENGTH, unique=False)
    pinyin = models.CharField(max_length=TITLE_MAX_LENGTH)
    english = models.CharField(max_length=TITLE_MAX_LENGTH)
    frequency = models.IntegerField(default=0)
    updated = models.DateTimeField('date updated', auto_now=True)
    category = models.CharField(max_length=TITLE_MAX_LENGTH)
    def __str__(self):
        return self.chinese


class UserSource(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'User Sources'

    TITLE_MAX_LENGTH = 255

    title = models.CharField(max_length=TITLE_MAX_LENGTH)  # e.g. book title, email subject
    author = models.CharField(max_length=TITLE_MAX_LENGTH)  # e.g. author or text
    type = models.CharField(max_length=TITLE_MAX_LENGTH)  # e.g. book, movie subs, emails
    url = models.URLField()
    # views = models.IntegerField(default=0)
    slug = models.SlugField(unique=True)

    # def save(self, *args, **kwargs):
    #
    #     self.slug = slugify(self.title)
    #     super(UserSource, self).save(*args, **kwargs)

    def save(self, **kwargs):
        slug_str = "%s %s" % (self.user, self.title)
        unique_slugify(self, slug_str)
        super(UserSource, self).save()

    def __str__(self):
        return self.title


class UserVocab(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'User Vocabulary'

    TITLE_MAX_LENGTH = 128
    # sources = models.ManyToManyField(UserSource)  # e.g. book title, email subject

    # def get_sources(self):
    #     return ",".join([str(p) for p in self.sources.all()])
    source = models.ForeignKey(UserSource, on_delete=models.CASCADE)
    category = models.ForeignKey(UserCategory, on_delete=models.CASCADE, related_name='category')
    hsk = models.ForeignKey(UserHSK, on_delete=models.CASCADE)  # , null=True, blank=True)
    chinese = models.CharField(max_length=TITLE_MAX_LENGTH, unique=False)
    pinyin = models.CharField(max_length=TITLE_MAX_LENGTH)
    english = models.CharField(max_length=TITLE_MAX_LENGTH)
    frequency = models.IntegerField(default=0)
    previous = models.IntegerField(default=0)
    score = models.IntegerField(default=0)
    learnt = models.IntegerField(default=0)
    hsk_lvl = models.IntegerField(default=0)
    slug = models.SlugField()

    # def save(self, *args, **kwargs):
    #
    #     self.slug = slugify(self.pinyin)
    #     super(UserVocab, self).save(*args, **kwargs)

    def save(self, **kwargs):
        slug_str = "%s %s %s" % (self.user, self.pinyin, self.english)
        unique_slugify(self, slug_str)
        super(UserVocab, self).save()

    def __str__(self):
        return self.chinese


class Sentence(models.Model):
    class Meta:
        verbose_name_plural = 'Sentences'
        ordering = ['number']

    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    chinese = models.TextField()
    pinyin = models.TextField()
    english = models.TextField()
    number = models.IntegerField(default=0)
    pub_date = models.DateTimeField('date published', auto_now=True)
    slug = models.SlugField()  # unique=True

    def save(self, *args, **kwargs):
        self.slug = slugify(self.pinyin)
        super(Sentence, self).save(*args, **kwargs)

    def __str__(self):
        return self.chinese


class UserSentence(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    source = models.ForeignKey(UserSource, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'User Sentences'
        ordering = ['number']

    chinese = models.TextField()
    pinyin = models.TextField()
    english = models.TextField()
    slug = models.SlugField()
    number = models.IntegerField(default=0)
    pub_date = models.DateTimeField('date published', auto_now=True)
    slug = models.SlugField()  # unique=True

    def save(self, *args, **kwargs):
        self.slug = slugify(self.pinyin)
        super(UserSentence, self).save(*args, **kwargs)

    def __str__(self):
        return self.chinese


class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def user_directory_path(instance, filename):
        # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
        return 'user_{0}/{1}'.format(instance.user.username, filename)

    # The additional attributes we wish to include.
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to=user_directory_path, blank=True)
    document = models.FileField(upload_to=user_directory_path, blank=True)

    def __str__(self):
        return self.user.username


class MotherText(models.Model):
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    mother = models.TextField()
    # chapter = models.IntegerField(default=0)
    number = models.IntegerField(default=0)
    pub_date = models.DateTimeField('date published', auto_now=True)
    slug = models.SlugField()  # unique=True

    class Meta:
        ordering = ['number']

    def save(self, *args, **kwargs):
        self.slug = slugify(self.mother)
        super(MotherText, self).save(*args, **kwargs)

    def __str__(self):
        return self.mother


class UserMotherText(models.Model):
    source = models.ForeignKey(UserSource, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    mother = models.TextField()
    number = models.IntegerField(default=0)
    pub_date = models.DateTimeField('date published', auto_now=True)
    slug = models.SlugField()  # unique=True

    def save(self, *args, **kwargs):
        self.slug = slugify(self.mother)
        super(UserMotherText, self).save(*args, **kwargs)

    def __str__(self):
        return self.mother


class word_inline(admin.TabularInline):
    model = Vocab
    extra = 1


class user_word_inline(admin.TabularInline):
    model = UserVocab
    extra = 1


class CategoryAdmin(admin.ModelAdmin):
    # prepopulated_fields = {'slug':('name',)}
    inlines = (word_inline,)


class HSKAdmin(admin.ModelAdmin):
    # prepopulated_fields = {'slug':('hsk_level',)}
    inlines = (word_inline,)


class UserCategoryAdmin(admin.ModelAdmin):
    # prepopulated_fields = {'slug':('name',)}
    inlines = (user_word_inline,)


class UserHSKAdmin(admin.ModelAdmin):
    # prepopulated_fields = {'slug':('hsk_level',)}
    inlines = (user_word_inline,)


def unique_slugify(instance, value, slug_field_name='slug', queryset=None,
                   slug_separator='-'):
    """
    Calculates and stores a unique slug of ``value`` for an instance.

    ``slug_field_name`` should be a string matching the name of the field to
    store the slug in (and the field to check against for uniqueness).

    ``queryset`` usually doesn't need to be explicitly provided - it'll default
    to using the ``.all()`` queryset from the model's default manager.
    """
    slug_field = instance._meta.get_field(slug_field_name)

    slug = getattr(instance, slug_field.attname)
    slug_len = slug_field.max_length

    # Sort out the initial slug, limiting its length if necessary.
    slug = slugify(value)
    if slug_len:
        slug = slug[:slug_len]
    slug = _slug_strip(slug, slug_separator)
    original_slug = slug

    # Create the queryset if one wasn't explicitly provided and exclude the
    # current instance from the queryset.
    if queryset is None:
        queryset = instance.__class__._default_manager.all()
    if instance.pk:
        queryset = queryset.exclude(pk=instance.pk)

    # Find a unique slug. If one matches, at '-2' to the end and try again
    # (then '-3', etc).
    next = 2
    while not slug or queryset.filter(**{slug_field_name: slug}):
        slug = original_slug
        end = '%s%s' % (slug_separator, next)
        if slug_len and len(slug) + len(end) > slug_len:
            slug = slug[:slug_len - len(end)]
            slug = _slug_strip(slug, slug_separator)
        slug = '%s%s' % (slug, end)
        next += 1

    setattr(instance, slug_field.attname, slug)


def _slug_strip(value, separator='-'):
    """
    Cleans up a slug by removing slug separator characters that occur at the
    beginning or end of a slug.

    If an alternate separator is used, it will also replace any instances of
    the default '-' separator with the new separator.
    """
    separator = separator or ''
    if separator == '-' or not separator:
        re_sep = '-'
    else:
        re_sep = '(?:-|%s)' % re.escape(separator)
    # Remove multiple instances and if an alternate separator is provided,
    # replace the default '-' separator.
    if separator != re_sep:
        value = re.sub('%s+' % re_sep, separator, value)
    # Remove separator from the beginning and end of the slug.
    if separator:
        if separator != '-':
            re_sep = re.escape(separator)
        value = re.sub(r'^%s+|%s+$' % (re_sep, re_sep), '', value)
    return value
