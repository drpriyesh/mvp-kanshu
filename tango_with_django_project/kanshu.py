# original script that works in terminal (not designed for web app).
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
import django
django.setup()

from rango.models import Category, Vocab, Sentence
from googletrans import Translator
from pinyin_jyutping_sentence import pinyin as p
from jieba import cut as j
from jieba import posseg as pseg
import re
import sqlite3
from split_into_sentences import split_into_sentences
from populate.CEdict_parser import main


def add_vocab(category, chinese, english, pinyin, learnt=0, score=0, previous=0, frequency=0):

    p = Vocab.objects.get_or_create(category=category, chinese=chinese, english=english, pinyin=pinyin)[0]
    p.score=score
    p.frequency=frequency
    p.previous=previous
    p.learnt=learnt
    p.save()
    return p


def add_cat(name):

    c = Category.objects.get_or_create(name=name)[0]
    c.save()
    return c


def add_sentence(chinese, pinyin, english, number=1):

    s = Sentence.objects.get_or_create(chinese=chinese, pinyin=pinyin, english=english)[0]
    s.number = number
    s.save()
    return s

CEdict = main()

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


flag_ext =  {"n": "Common nouns",
             "f": "orientation noun",
             "s": "premises noun",
             "t": "time",
             "nr": "name",
             "ns": "place names",
             "nt": "organization name",
             "nw": "title",
             "nz": "Other proper names",
             "v": "Common verbs",
             "vd": "verb",
             "vn": "noun verb",
             "a": "adjective",
             "ad": "adverb",
             "an": "nouns",
             "d": "adverb",
             "m": "quantifier",
             "q": "quantifier",
             "r": "pronoun",
             "p": "preposition",
             "c": "conjunction",
             "u": "particle",
             "xc": "other word",
             "w": "punctuation",
             "PER": "name",
             "LOC": "Place Name",
             "ORG": "Organization Name",
             "TIME": "Time"}

translator = Translator()
g = translator.translate

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEXT_DIR = os.path.join(BASE_DIR, 'tango_with_django_project/static/texts/')
print(TEXT_DIR)


# inputPath = "/Users/priyeshpatel/PycharmProjects/rango/tango_with_django_project/static/texts/"
# transPath = "/Users/priyeshpatel/PycharmProjects/rango/tango_with_django_project/static/texts/"

with open(TEXT_DIR + 'Harry_Potter_Mandarin.txt', encoding="utf8", errors='ignore') as f:
    inputSample = f.read()

with open(TEXT_DIR + 'Harry_Potter_English.txt', encoding="utf8", errors='ignore') as ff:
    transSample = ff.read()

inputSample_list = j(inputSample, cut_all=False)
iTok = split_into_sentences(inputSample)
tTok = split_into_sentences(transSample)

threshold = 3
learnMode = 1
count = 0

for sentence in iTok:

    # categorize
    posDict = {}
    words = pseg.cut(sentence)
    for w in words:
        posDict[w.word] = w.flag

    # pinyin
    pYa = p(sentence, spaces=False)
    pYa = pYa.replace(',', ' ')
    pYa = pYa.replace('  ', '')
    pYa = pYa.strip(' .')
    pYa = pYa.replace(' ', ',')
    pY = pYa.replace(',', '\t') # this is pinyin sentence for display

    # google trans
    gTranslated = g(sentence).text # this is google translated sentence for display

    # jieba
    seg_list = j(sentence, cut_all=False)
    segStr = (",".join(seg_list))
    segStr = segStr.replace(',', ' ')
    segStr = segStr.replace('  ', '')
    segStr = segStr.strip(' .')
    segStr = segStr.replace(' ', ',')
    segSent = (segStr.replace(',', '\t')) # this is sentence for display
    print("\n")
    print(bcolors.HEADER + segSent + bcolors.ENDC)
    print("\n")

    # making list of words
    # pinyin list
    pyList = pYa.split(",")
    # chinese list
    segStrList = segStr.split(",")

    statD = {}
    vocD = []



    # iterating through words in sentence
    for word in segStrList:
        if re.search("[\u4e00-\u9FFF]", word):
            gT = g(word)
            pYw = p(word)
            curr_cat = posDict[word]
            try:
                curr_cat = flag_ext[curr_cat]
            except KeyError:
                curr_cat = 'other'

            # checking if word exists in db and get data or make new addition
            try:
                data = Vocab.objects.get(chinese=word)
            except data.DoesNotExist:
                data = None

            if data is None:
                print('There is no word %s in db' % word)

                # adding new word to db
                myDict = {'category': curr_cat, 'chinese': word, 'english': gT.text, 'pinyin': pYw, 'frequency': 1, 'previous': 1, 'score': 0, 'learnt': 0}
                c = add_cat(myDict['category'])
                add_vocab(c, myDict['chinese'], myDict['english'], myDict['pinyin'], myDict['learnt'], myDict['score'], myDict['previous'],
                          myDict['frequency'])
                data = Vocab.objects.get(chinese=word)

            else:
                print('Word %s found in db' %(word))   # with %s' % (word, data))

            score = data.score
            data.previous = data.previous + 1
            data.save()

            # enter special learning mode that allows user to increase word score
            if score < threshold and learnMode == 1:

                l0 = gT.origin
                print(bcolors.HEADER + l0 + bcolors.ENDC + " : Enter 1-3 for how well you know this? (negative int resets to 0): ")

                known = int(0)
                while True:
                    try:
                        known = int(input())
                    # If something else that is not the string
                    # version of a number is introduced, the
                    # ValueError exception will be called.
                    except ValueError:
                        # The cycle will go on until validation
                        print(bcolors.FAIL + "Error! This is not a number. Try again." + bcolors.ENDC)
                    # When successfully converted to an integer,
                    # the loop will end.
                    else:
                        if 0 < known < 4 or known < 0:  # this is faster
                            if known < 0:
                                data.score = 0
                                data.save()
                            else:
                                data.score = known + score
                                data.save()
                            break
                        else:
                            print(bcolors.FAIL + "Out of range. Try again" + bcolors.ENDC)

                if known < 3:
                    print("\n")
                    l1 = gT.origin + "->" + gT.text
                    l2 = pYw
                    print(bcolors.OKGREEN + gT.origin + bcolors.ENDC + ' - ' + bcolors.WARNING + gT.text + bcolors.ENDC)
                    print(bcolors.OKBLUE + l2 + bcolors.ENDC)
                    print("\n")


    import textwrap

    # Wrap this text.
    wrapper = textwrap.TextWrapper(width=100)

    a = ','.join(segStrList)
    a = a.replace(',', ' ')
    b = ','.join(pyList)
    b = b.replace(',', ' ')
    aa = [match.start() for match in re.finditer(r'\b\w', a)]
    bb = [match.start() for match in re.finditer(r'\b\w', b)]
    elong = []
    l = []
    pp = 0
    for idx, item in enumerate(aa):
        cc = bb[idx] - (item + pp)
        pp = pp+cc
        # print(pp)
        # print(cc)
        # print(segStrList[idx])
        elong = segStrList[idx].rjust(cc)
        # print(elong)
        l.append(elong)
        # print(l)

    l4 = ','.join(l)
    l4 = l4.replace(',', ' ')
    l4 = segSent # chinese aligned sentence

    print('\n')
    print(bcolors.OKGREEN + l4 + bcolors.ENDC)

    l5 = pYa.replace(',', '\t') # pinyin aligned sentence
    l6 = gTranslated # google translated sentence

    sentence_count = 1
    add_sentence(l4, l5, l6, sentence_count)
    sentence_count = sentence_count + 1

    print(bcolors.OKBLUE + bcolors.BOLD + l5 + bcolors.ENDC)
    print(bcolors.WARNING + l6 + bcolors.ENDC)
    print('\n')

    if count > 0:

        tex = tTok[count-1] + ' ' + bcolors.BOLD + bcolors.WARNING + tTok[count] + bcolors.ENDC + ' ' + tTok[count+1]
    else:

        tex = bcolors.BOLD  + tTok[count] + bcolors.ENDC + ' ' + tTok[count+1]

    tex = tex # wider text section from book

    word_list = wrapper.wrap(text=tex)

    # Print each line.
    for element in word_list:
        print(element)

    print("Current The_Little_Princecount number:", count)

    while True:
        try:
            countChk = int(input("Adjust The_Little_Princecount by?"))
        # If something else that is not the string
        # version of a number is introduced, the
        # ValueError exception will be called.
        except ValueError:
            # The cycle will go on until validation
            # print("Error! This is not a number. Try again.")
            print(bcolors.FAIL + "Error! This is not a number. Try again." + bcolors.ENDC)
            # When successfully converted to an integer,
            # the loop will end.
        else:
            if -9 < countChk < 9:  # this is faster
                break
            else:
                print(bcolors.FAIL + "Out of range (-9 to 9). Try again" + bcolors.ENDC)

    count = count+countChk+1
# need handle end of sentence .... doesnt crap out...

