import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
django.setup()

from rango.models import Category, Vocab, Sentence, MotherText, Source
from split_into_sentences import split_into_sentences
import string

def add_source(name):
    s = Source.objects.get_or_create(title=name)[0]
    s.save()
    return s

def add_mother(source, mother, number):

    m = MotherText.objects.get_or_create(source=source, mother=mother)[0]
    m.number = number
    m.save()
    return m

# filename = "Harry_Potter_English"
filename = input("Enter filename (OR press enter for The_Little_Prince_English): ") or "The_Little_Prince_English"

print(filename)
s = add_source(filename)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEXT_DIR = os.path.join(BASE_DIR, 'tango_with_django_project/static/texts/')

with open(TEXT_DIR + filename + '.txt', encoding="utf8", errors='ignore') as ff:
    transSample = ff.read()

tTok = split_into_sentences(transSample)

sentence_count = 1
create_list=[]
for sentence in tTok:

    if len(sentence) < 3:
        sentence = " "
    sdict={'source':s, 'mother':sentence, 'number':sentence_count}
    create_list.append(sdict)
    # add_mother(s, sentence, sentence_count)
    sentence_count = sentence_count + 1
    print(str(sentence_count) + ":" + sentence)

if create_list:
    batch = 200
    q = len(create_list) // batch
    r = len(create_list) % batch
    ss = len(create_list) - r
    if len(create_list) > batch:


        for i in range(0,q):
            start = batch*i
            end = (batch*i)+batch
            print('q:'+str(q) +' i:'+str(i) +' start:'+ str(start) + ' -> end:'+ str(end))
            MotherText.objects.bulk_create([MotherText(**kv) for kv in create_list[start:end]])

    print('last start:' + str(ss) + '-> end:' + str(len(create_list)))

    MotherText.objects.bulk_create([MotherText(**kv) for kv in create_list[ss:len(create_list)]])