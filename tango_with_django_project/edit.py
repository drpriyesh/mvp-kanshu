# " jieba.load_userdict(file_name) # file_name is the path dictionary format of a file-type object or a custom dictionary.
# It is the same as dict.txt. One word Occupy one line; each line is divided into three parts: words, word frequency (can be omitted),
# part of speech (can be omitted), separated by spaces, and the order cannot be reversed.
# If file_name is a file opened in path or binary mode, the file must be UTF-8 encoded.
# When the word frequency is omitted, the automatically calculated word frequency that can guarantee the separation of the word is used."

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
import django
django.setup()
from rango.models import Sentence, Source, HSK, Category, Vocab
import jieba
from jieba import cut as j
import jieba.posseg as pseg
from pinyin_jyutping_sentence import pinyin as pinyin


def edit(source, words):
    update_list = []
    s = Source.objects.get(title=source)
    # jieba.enable_paddle()

    dict_path = "C:/Users/bzgl6w/PycharmProjects/rango/tango_with_django_project/rango/customdict.txt"
    jieba.load_userdict(dict_path)
    for row in words:
        word = row['word']
        pin = pYa = pinyin(word, spaces=False)

        # freq = int(word['freq'])
        tag = row['tag']
        english = tag+': '+row['english']
        h = HSK.objects.get_or_create(hsk_level=0)[0]
        h.save()
        c = Category.objects.get_or_create(name=tag)[0]
        c.save()
        v = Vocab.objects.get_or_create(source=s, category=c, hsk=h, chinese=word, pinyin=pin, english=english)[0]
        v.save()
        jieba.add_word(word, freq=None, tag=tag)

    olist = list(Sentence.objects.filter(source=s).values('id', 'source', 'chinese', 'pinyin'))


    plist = list(Sentence.objects.filter(source=s).values('id', 'source', 'chinese', 'pinyin'))
    # print(plist[:10])
    count=0
    for line in plist:

        oline = olist[count]
        # print(line['chinese'])
        l = line['chinese']
        l = l.replace('\t', '')
        # print(l)
        l = l.replace('\t', '')

        # print(l)
        seg_list = jieba.cut(l, cut_all=False)
        segStr = (",".join(seg_list))
        segStr = segStr.replace(',', ' ')
        segStr = segStr.replace('  ', '')
        segStr = segStr.strip(' .')
        segStr = segStr.replace(' ', ',')
        segSent = (segStr.replace(',', '\t'))  # this is chinese sentence for display
        line['chinese']=segSent
        # print(line['chinese'])
        # print(oline['chinese'])
        pYa = pinyin(l, spaces=False)
        pYa = pYa.replace(',', ' ')
        pYa = pYa.replace('  ', '')
        pYa = pYa.strip(' .')
        pYa = pYa.replace(' ', ',')
        pY = pYa.replace(',', '\t')
        line['pinyin'] = pY


        if segSent != oline['chinese']:

            # print(oline['chinese'])
            # print(line['chinese'])
            update_dict = {'id': oline['id'], 'chinese': line['chinese'], 'pinyin': line['pinyin']}
            update_list.append(update_dict)
        count+=1


    if update_list:
        batch = 100
        if len(update_list) > batch:

            q = len(update_list) // batch
            r = len(update_list) % batch
            s = len(update_list)-r
            for i in range(0,q):
                start = batch*i
                end = (batch*i)+batch
                print('q:'+str(q) +' i:'+str(i) +' start:'+ str(start) + ' -> end:'+ str(end))
                Sentence.objects.bulk_update([Sentence(**kv) for kv in update_list[start:end]], ['chinese', 'pinyin'])

        print('last start:' + str(s) + '-> end:' + str(len(update_list)))

        Sentence.objects.bulk_update([Sentence(**kv) for kv in update_list[s:len(update_list)]], ['chinese', 'pinyin'])

    print('done')

if __name__=='__main__':

    print('Starting Kanshu sentence edit script...')
    words = []
    words.append({'word':'达德里', 'freq':'10000000000', 'tag':'Name', 'english':'Dudley'})
    words.append({'word':'哈利波特', 'freq':'1000000000000', 'tag':'Name', 'english':'Harry Potter'})
    words.append({'word':'哈格力', 'freq':'1000000000000', 'tag':'Name', 'english':'Hagrid'})
    print(words)

    edit("Harry_Potter_Mandarin", words)