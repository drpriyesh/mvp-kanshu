//$('#search-input').keyup(function() {
//var query;
//query = $(this).val();
//$.get('/rango/suggest/',
//{'suggestion': query},
//function(data) {
//$('#categories-listing').html(data);
//})
//});


$(document).ready(
function() {
$('#like_btn').click(
function() {
var categoryIdVar;
categoryIdVar = $(this).attr('data-categoryid');
$.get(
'/rango/like_category/', {'category_id': categoryIdVar}, function(data) {
$('#like_count').html(data);
$('#like_btn').hide();
})
}); });



$('#bookmark_btn').on('click',
function() {
var bookmarkIdVar;
bookmarkIdVar = $(this).attr('data-categoryid7');
sourceIdVar = $(this).attr('data-categoryid8');
$.get(
'/rango/bookmark/', {'bookmark': bookmarkIdVar, 'source_title_slug': sourceIdVar}, function(data) {
$('#bookmark_num').html(data);
$('#bookmark_btn').hide();
})
});

$(document).on('click touchstart tap', 'button[data-id]', function (e) {
var requested_to = $(this).attr('data-id');
//console.log(requested_to);
$.get(
'/rango/learnt/', {'result_id': requested_to}, function(data) {
$('#learnt_count').html(data);
$('.' + requested_to).hide();
$('.toast' + requested_to).toast('show');
$('.svg' + requested_to).css({ fill: "#777" });
})
});


$('button[data-categoryid1]').on('click',
function() {
var chineseIdVar;
chineseIdVar = $(this).attr('data-categoryid1');
pinyinIdVar = $(this).attr('data-categoryid2');
englishIdVar = $(this).attr('data-categoryid3');
sourceIdVar = $(this).attr('data-categoryid4');
$.get(
'/rango/add_word2dict/', {'chinese': chineseIdVar, 'pinyin': pinyinIdVar,'english': englishIdVar, 'source_title_slug': sourceIdVar}, function(data) {
$('#add2dict_num').html(data);
$('#add2dict_btn').hide();
})

});


$('button[data-categoryid9]').on('click',
function() {
var chineseIdVar;
chineseIdVar = $(this).attr('data-categoryid9');
pinyinIdVar = $(this).attr('data-categoryid10');
englishIdVar = $(this).attr('data-categoryid11');
sourceIdVar = $(this).attr('data-categoryid12');
$.get(
'/rango/del_word2dict/', {'chinese': chineseIdVar, 'pinyin': pinyinIdVar,'english': englishIdVar, 'source_title_slug': sourceIdVar}, function(data) {
$('#del2dict_num').html(data);
window.location.reload();
})
});




$('button[data-categoryid5]').on('click',
function() {
var wordIdVar;
wordIdVar = $(this).attr('data-categoryid5');
sourceIdVar = $(this).attr('data-categoryid6');
$.get(
'/rango/words_in_sent/', {'word': wordIdVar, 'source': sourceIdVar}, function(data) {
$('#sentence_data').html(data);
//$('#sentence_btn').hide();
})
});