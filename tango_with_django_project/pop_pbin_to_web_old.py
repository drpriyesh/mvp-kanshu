# takes the vocab stored in a bin file (pickled dict) and places it into database
# need to update to include Source model

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
import django
django.setup()

from populate.pickle_dat import unpickling
from rango.models import Category, Vocab, HSK, Source
from pop3 import add_vocab, add_cat, add_hsk, add_source
import time
import collections

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEXT_DIR = os.path.join(BASE_DIR, 'tango_with_django_project/static/texts/')
POP_DIR = os.path.join(BASE_DIR, 'tango_with_django_project/populate/')

# filename = "Harry_Potter_Mandarin"
filename = input("Enter filename (OR press enter for Harry_Potter_Mandarin): ") or "Harry_Potter_Mandarin"
                                                                                        # "The_Little_Prince_Mandarin"
print(filename)
# vPath = '/Users/priyeshpatel/PycharmProjects/rango/tango_with_django_project/populate/'
vocab = unpickling(POP_DIR + filename + 'vocab')['data']
# print("original")
# print(vocab[1:11])


hsk = {}


with open(POP_DIR + 'HSK_Official_2012_L1.txt', encoding="utf8", errors='ignore') as f1:
    text = f1.read()
    lines = text.split('\n')
    dict_lines = list(lines)
    hsk['1'] = dict_lines
with open(POP_DIR + 'HSK_Official_2012_L2.txt', encoding="utf8", errors='ignore') as f2:
    text = f2.read()
    lines = text.split('\n')
    dict_lines = list(lines)
    hsk['2'] = dict_lines
with open(POP_DIR + 'HSK_Official_2012_L3.txt', encoding="utf8", errors='ignore') as f3:
    text = f3.read()
    lines = text.split('\n')
    dict_lines = list(lines)
    hsk['3'] = dict_lines
with open(POP_DIR + 'HSK_Official_2012_L4.txt', encoding="utf8", errors='ignore') as f4:
    text = f4.read()
    lines = text.split('\n')
    dict_lines = list(lines)
    hsk['4'] = dict_lines
with open(POP_DIR + 'HSK_Official_2012_L5.txt', encoding="utf8", errors='ignore') as f5:
    text = f5.read()
    lines = text.split('\n')
    dict_lines = list(lines)
    hsk['5'] = dict_lines
with open(POP_DIR + 'HSK_Official_2012_L6.txt', encoding="utf8", errors='ignore') as f6:
    text = f6.read()
    lines = text.split('\n')
    dict_lines = list(lines)
    hsk['6'] = dict_lines


vocab_hsk = []

print("Adding hsk level to vocab dict...")
for i in range(1,7):
    for word in hsk[str(i)]:
        for item in vocab:
            # item['hsk'] = 0
            # print(item['chinese'] + " : " + word)

            if item['chinese'] == word:
                item['hsk'] = i
                # wait = input("PRESS ENTER TO CONTINUE.")
            # print(item)
            # time.sleep(0.1)
                vocab_hsk.append(item)


# print("\n\nhsk")
# print(vocab_hsk[1:11])

# print("Grouping vocabulary to hsk level...")
# groupedHSK = collections.defaultdict(list)
# for item in vocab_hsk:
#     groupedHSK[item['hsk']].append(item)
#
# hsks = {}
# for hsk, group in groupedHSK.items():
#     hsks[hsk] = {'vocabulary': group}
#
# for hsk, hsk_data in hsks.items():
#     # print(hsk_data)
#     h = add_hsk(hsk)
#
#     for p in hsk_data['vocabulary']:
#
#         add_vocab(p['category'], p['chinese'], p['english'], p['pinyin'], p['learnt'], p['score'], p['previous'], p['frequency'], h)

flag_ext2 = {"n": "Common nouns",
            "f": "Orientation noun",
            "s": "Premises noun",
            "t": "Time",
            "nr": "Name",
            "ns": "Place names",
            "nt": "Organization name",
            "nw": "Title",
            "nz": "Other proper names",
            "v": "Common verbs",
            "vd": "Verb",
            "vn": "Noun verb",
            "a": "Adjective",
            "ad": "Adverb",
            "an": "Nouns",
            "d": "Adverb",
            "m": "Quantifier",
            "q": "quantifier",
            "r": "Pronoun",
            "p": "Preposition",
            "c": "Conjunction",
            "u": "Particle",
            "xc": "Other word",
            "w": "Punctuation",
            "PER": "Name",
            "LOC": "Place Name",
            "ORG": "Organization Name",
            "TIME": "Time"}

flag_ext = {'j': 'Abbreviations',
            'g': 'Academic vocabulary',
            'a': 'Adjective',
            'al': 'Adjective idiom',
            # 'ag':'Adjective morpheme',
            'ad': 'Adverb',
            'd': 'Adverb',
            'vd': 'Adverb',
            'ag': 'Adverbial',
            'dg': 'Adverbial',
            # 'dg':'Adverbs',
            'nic': 'Affiliates',
            'nis': 'Agency suffix',
            'Rg': 'Ancient Chinese pronoun morpheme',
            'nba': 'Animal name',
            'uyy': 'As common as',
            'ntcb': 'Bank',
            'nb': 'Biological name',
            'gb': 'Biology related words',
            'gbc': 'Biotype',
            'nbp': 'Botanical name',
            'ude1': 'Bottom of',
            'nmc': 'Chemical name',
            'gc': 'Chemistry related vocabulary',
            'wm': 'Colon, full-width :: half-width::',
            'wd': 'Comma, full-width :, half-width :,',
            'ntc': 'Company Name',
            'nr1': 'Compound surname',
            'gi': 'Computer related vocabulary',
            'c': 'Conjunction',
            'ul': 'Conjunction',
            'uv': 'Conjunction',
            'cc': 'Coordinate conjunction',
            'wp': 'Dash, full angle: —— —— —— － half angle: —— —',
            'rz': 'Demonstrative',
            'nhd': 'Disease',
            'bg': 'Distinctive morpheme',
            'bl': 'Distinguish part of speech idioms',
            'b': 'Distinguishing words',
            'nhm': 'Drug',
            'nit': 'Education related institutions',
            'nts': 'Elementary and secondary schools',
            'ws': 'Ellipses, full-width: ...',
            'wt': 'Exclamation mark, full-width :!',
            'ntcf': 'Factory',
            'nf': 'Food, such as "potato chips"',
            'vx': 'Formal verb',
            'wj': 'Full stop, full angle :.',
            'gg': 'Geology and Geology',
            'ude3': 'Get',
            'nto': 'Government agency',
            'ude2': 'Ground',
            'nh': 'Health related terms such as medical diseases',
            'nth': 'Hospital',
            'ntch': 'Hotel Guest House',
            'i': 'Idiom',
            'l': 'Idiom',
            'udh': 'if',
            'uls': 'In terms of speaking',
            # 'nt':'Institution name',
            'nt': 'Institutional groups',
            'ni': 'Institution-related (not the name of an independent institution)',
            'e': 'Interjection',
            'rys': 'Interrogative pronoun',
            'ry': 'Interrogative pronouns',
            'vi': 'Intransitive verb (internal verb)',
            'nm': 'Item name',
            'nrj': 'Japanese name',
            'nn': 'Job-related nouns',
            'wkz': 'Left bracket, full angle: (〔[{{"" [〖<half angle: ([{<',
            # 'wyz':'Left quote, full angle: "'『',
            'nx': 'Letter proper name',
            'ulian': 'Lian ("Even Primary School Students")',
            'dl': 'Link',
            's': 'Location word',
            'gm': 'Math related vocabulary',
            # 'y':'Modal',
            'y': 'Modal particle (delete yg)',
            'nr2': 'Mongolian name',
            # 'g':'Morpheme',
            # 'vg':'Morpheme',
            'nr': 'Name',
            'an': 'Nominal form',
            'nl': 'Nominal idiom',
            'ng': 'Nominal morpheme',
            'vn': 'Nominal verb',
            # 'x':'Non-morpheme',
            'xx': 'Non-morpheme',
            'n': 'Noun',
            'mg': 'Number morpheme',
            'm': 'Numeral',
            'Mg': 'Numerals like A, B, C',
            'nnd': 'Occupation',
            'uzhi': 'Of',
            'ule': 'Okay',
            'end': 'Only for final ## Final',
            'o': 'Onomatopoeia',
            'nz': 'Other proper names',
            'u': 'Particle',
            'ud': 'Particle',
            'uj': 'Particle',
            'ug': 'Past',
            'uguo': 'Past',
            'wb': 'Percent sign, thousand sign, full angle:% ‰ half angle:%',
            'rr': 'Personal Pronouns',
            'gp': 'Physics related vocabulary',
            'usuo': 'Place',
            'ns': 'Place name',
            'nnt': 'Position / Title',
            'f': 'Position of the word',
            # 'h':'Predecessor',
            'rzv': 'Predicate demonstrative pronoun',
            'ryv': 'Predicate interrogative pronoun',
            'h': 'Prefix',
            'rzs': 'Premise pronoun',
            'p': 'Preposition',
            'r': 'Pronoun',
            'rg': 'Pronoun morpheme',
            'w': 'Punctuation',
            'q': 'quantifier',
            'mq': 'Quantifier1',
            'qg': 'Quantifier2',
            'qt': 'Quantifier3',
            'qv': 'Quantifier4',
            'ww': 'Question mark, full-width :?',
            'wky': 'Right parenthesis, full-width :)〕]｝》】〉 Half-width:)] {>',
            # 'wyy':'Right quote, full-width: "'』',
            'wf': 'Semicolon, full-width :; half-width :;',
            'z': 'Status word',
            'zg': 'Status word',
            'x': 'String',
            # 'k':'Subsequent ingredients',
            'k': 'Suffix',
            'tg': 'Temporal part-of-speech',
            # 'tg':'Tenor',
            'pba': 'The preposition "Ba"',
            'pbei': 'The preposition "be"',
            'ntu': 'the University',
            'vyou': 'The verb "有"',
            'ryt': 'Time interrogative pronoun',
            'rzt': 'Time pronoun',
            't': 'Time word',
            'wn': 'Ton, full-width :,',
            'yg': 'Tone morpheme',
            'nsf': 'Transliterated place names',
            'nrf': 'Transliteration',
            'vf': 'Trending verb',
            'wh': 'Unit symbol, full-width: ¥ ＄ ￡ ° ℃ half-width: $',
            'un': 'Unknown word',
            'xu': 'URL URL',
            'v': 'Verb',
            'vshi': 'Verb "Yes"',
            'vl': 'Verb idiom',
            'vg': 'Verb morpheme',
            'udeng': 'Wait wait wait',
            'uz': 'Write',
            'uzhe': 'Write',
            'nbc': 'Zoology'
            }

print("Grouping vocabulary to categories...")
grouped = collections.defaultdict(list)
for item in vocab:
    grouped[item['category']].append(item)

cats = {}
for category, group in grouped.items():
    cats[category] = {'vocabulary': group}

s = add_source(filename)
print("Adding categories and vocabulary to db...")
for cat, cat_data in cats.items():
    try:
        cat = flag_ext[cat]
    except KeyError:
        cat = 'other'
    c = add_cat(cat)
    c.sources.add(s)
    c.save()

    for p in cat_data['vocabulary']:
        if "hsk" not in p:
            p['hsk'] = '0'
        h = add_hsk(p['hsk'])
        v = add_vocab(s, c, h, p['chinese'], p['english'], p['pinyin'], p['learnt'], p['score'], p['previous'], p['frequency'], p['hsk'])
        v.save()

# Print out the categories we have added.
for c in Category.objects.all():
    for p in Vocab.objects.filter(category=c):
        print(f'- {c}: {p}')



