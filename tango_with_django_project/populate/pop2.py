import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
import django
django.setup()

from rango.models import Category, Vocab
from requests.exceptions import ConnectionError
from collections import Counter
from jieba import cut as j
from jieba import posseg as pseg


from googletrans import Translator
from pinyin_jyutping_sentence import pinyin as pinyin
import re
import time


from pprint import pprint


import collections




def populate():


    inputPath = "/Users/priyeshpatel/Downloads/HarryPMan.txt"
    with open(inputPath, encoding="utf8", errors='ignore') as f:
        inputSample = f.read()

    inputSample_list = j(inputSample, cut_all=False)
    inputSampleCnt = dict(Counter(inputSample_list))

    posDict = {}
    words = pseg.cut(inputSample)
    print('Identifying categories...')
    for w in words:
        posDict[w.word] = w.flag
        # print('%s %s' % (w.word, w.flag))


    sorted_inputSampleCnt = sorted(inputSampleCnt.items(), key=lambda kv: kv[1], reverse=True)
    vocab =[]
    count = 0

    # f = open("/Users/priyeshpatel/Downloads/guru99.txt", "w+")
    # while True:
    #     try:
    print("Creating vocabulary dictionary...")
    for x, freq in sorted_inputSampleCnt:

        if re.search("[\u4e00-\u9FFF]", x):

            count += 1
            if int(picklecount) < count:
                translator = Translator()
                g = translator.translate
                pY = pinyin(x, spaces=False)
                k = g(x).text

                try:
                    curr_cat = posDict[x]
                except KeyError:
                    curr_cat = 'other'

                att = {'category': curr_cat, 'chinese': x, 'score': 0, 'frequency': int(freq), 'english': k, 'previous': 0, 'pinyin': pY, 'learnt': 0}
                vocab.append(att)

                time.sleep(5)
                # print(att)
        # except ValueError:
        #     print('Google translate prob. blocked IP')
        # except ConnectionError as e:  # This is the correct syntax
        #     print(e)
    print(vocab)

    print("Grouping vocabulary to categories...")

    grouped = collections.defaultdict(list)
    for item in vocab:
        grouped[item['category']].append(item)
    cats = {}
    for category, group in grouped.items():
        print('')
        print(category)
        pprint(group, width=150)

        cats[category] = {'vocabulary': group}

    print(cats)


    for cat, cat_data in cats.items():
        c = add_cat(cat)
        for p in cat_data['vocabulary']:
            add_vocab(c, p['chinese'], p['english'], p['pinyin'], p['learnt'], p['score'], p['previous'], p['frequency'])
    # Print out the categories we have added.
    for c in Category.objects.all():
        for p in Vocab.objects.filter(category=c):
            print(f'- {c}: {p}')


def add_vocab(category, chinese, english, pinyin, learnt=0, score=0, previous=0, frequency=0):
    p = Vocab.objects.get_or_create(category=category, chinese=chinese, english=english, pinyin=pinyin)[0]
    p.score=score
    p.frequency=frequency
    p.previous=previous
    p.learnt=learnt
    p.save()
    return p


def add_cat(name):

    c = Category.objects.get_or_create(name=name)[0]
    c.save()
    return c


# Start execution here!
if __name__=='__main__':

    print('Starting Rango population script...')
    populate()