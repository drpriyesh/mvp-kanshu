import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
# import django
# django.setup()
from rango.models import Word

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEXT_DIR = os.path.join(BASE_DIR, 'static/texts/')
POP_DIR = os.path.join(BASE_DIR, 'populate/')
print(TEXT_DIR)
print(POP_DIR)
# CEDICT_PATH = "/Users/priyeshpatel/PycharmProjects/rango/tango_with_django_project/populate/cedict_ts.u8.txt"
CEDICT_PATH = POP_DIR + "cedict_ts.u8.txt"


with open(CEDICT_PATH, encoding="utf8", errors='ignore') as file:
    text = file.read()
    lines = text.split('\n')
    dict_lines = list(lines)

list_of_dicts = []
    # define functions

def add_word(traditional, simplified, english, pinyin):
    w = Word.objects.get_or_create(traditional=traditional, simplified=simplified, english=english, pinyin=pinyin)[0]
    w.save()
    return w

def parse_line(line):

    parsed = {}
    if line == '':
        dict_lines.remove(line)
        return 0
    line = line.rstrip('/')
    line = line.split('/')
    if len(line) <= 1:
        return 0
    english = line[1]
    char_and_pinyin = line[0].split('[')
    characters = char_and_pinyin[0]
    characters = characters.split()
    traditional = characters[0]
    simplified = characters[1]
    pinyin = char_and_pinyin[1]
    pinyin = pinyin.rstrip()
    pinyin = pinyin.rstrip("]")
    parsed['traditional'] = traditional
    parsed['simplified'] = simplified
    parsed['pinyin'] = pinyin
    parsed['english'] = english
    list_of_dicts.append(parsed)


def remove_surnames():
    for x in range(len(list_of_dicts) - 1, -1, -1):
        if "surname " in list_of_dicts[x]['english']:
            if list_of_dicts[x]['traditional'] == list_of_dicts[x + 1]['traditional']:
                list_of_dicts.pop(x)


def main():

    # make each line into a dictionary
    print("Parsing dictionary...")
    for line in dict_lines:
        parse_line(line)

    # remove entries for surnames from the data (optional):

    print("Removing Surnames...")
    remove_surnames()

    # If you want to save to a database as JSON objects, create a class Word in the Models file of your Django project:

    # print("Saving to database (this may take a few minutes) . . .")
    # for one_dict in list_of_dicts:
    #     new_word = add_word(traditional = one_dict["traditional"], simplified = one_dict["simplified"], english = one_dict["english"], pinyin = one_dict["pinyin"])
    #     # new_word.save()
    # print('Done!')

    return list_of_dicts

# list_of_dicts = []
# parsed_dict = main()

# Start execution here!
if __name__=='__main__':
    list_of_dicts = []
    print('Starting CEdict parser script...')
    main()
    print(list_of_dicts)

