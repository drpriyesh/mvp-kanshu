import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')
import django
django.setup()

from rango.models import Category, Attrib
from requests.exceptions import ConnectionError
from collections import Counter
from jieba import cut as j
from jieba import posseg as pseg
from collections import defaultdict

from googletrans import Translator
from pinyin_jyutping_sentence import pinyin as pinyin
import re
import time
import sqlite3

from pprint import pprint
import itertools




def populate():
    translator = Translator()
    g = translator.translate

    inputPath = "/Users/priyeshpatel/Downloads/HarryPMan.txt"
    with open(inputPath, encoding="utf8", errors='ignore') as f:
        inputSample = f.read()

    inputSample_list = j(inputSample, cut_all=False)
    inputSampleCnt = dict(Counter(inputSample_list))

    posDict = {}
    words = pseg.cut(inputSample, cut_all=False)
    for w in words:
        posDict[w.word] = w.flag
        print('%s %s' % (w.word, w.flag))

    res = defaultdict(list)
    for key, val in sorted(posDict.items()):
        res[val].append(key)

    sorted_inputSampleCnt = sorted(inputSampleCnt.items(), key=lambda kv: kv[1], reverse=True)
    words = []
    count = 0

    f = open("/Users/priyeshpatel/Downloads/guru99.txt", "w+")
    # while True:
    #     try:
    for x, freq in sorted_inputSampleCnt:

        if re.search("[\u4e00-\u9FFF]", x):

            count += 1
            if 2972 < count < 3100:
                translator = Translator()
                g = translator.translate
                pY = pinyin(x, spaces=False)
                k = g(x).text
                curr_cat = posDict[x]
                f.write(str(count) + "\t" + x + "\t" + pY + "\t" + k + "\t" + str(freq) + "\r\n")
                att = {'category': curr_cat, 'chinese': x, 'score': 0, 'frequency': int(freq), 'english': k, 'previous': 0, 'pinyin': pY, 'learnt': 0}
                words.append(att)

                time.sleep(0)
                # print(att)
        # except ValueError:
        #     print('Google translate prob. blocked IP')
        # except ConnectionError as e:  # This is the correct syntax
        #     print(e)

    def keyfunc(x):

        return x['category']

    SOME_DATA = sorted(words, key=keyfunc)
    for category, group in itertools.groupby(SOME_DATA, keyfunc):
        print('')
        print(category)
        pprint(list(group), width=150)

        cats = {category: (list(group))}


    # If you want to add more categories or pages, # add them to the dictionaries above.
    # The code below goes through the cats dictionary, then adds each category,
    # and then adds all the associated pages for that category.
    for cat, att_data in cats.items():

        category = add_cat(cat)
        # print(att_data)
        a = att_data['attributes']
        # print(a)
        add_att(category, a['chinese'], a['english'], a['pinyin'], a['learnt'], a['score'], a['previous'], a['frequency'])

    # Print out the categories we have added.
    for c in Category.objects.all():
        for p in Attrib.objects.filter(category=c):
            print(f'- {c}: {p}')

def add_att(category, chinese, english, pinyin, learnt=0, score=0, previous=0, frequency=0):
    p = Attrib.objects.get_or_create(category=category, chinese=chinese, english=english, pinyin=pinyin)[0]
    p.score=score
    p.frequency=frequency
    p.previous=previous
    p.learnt=learnt
    p.save()
    return p


def add_cat(name):

    c = Category.objects.get_or_create(name=name)[0]
    c.save()
    return c


# Start execution here!
if __name__=='__main__':

    print('Starting Rango population script...')
    populate()